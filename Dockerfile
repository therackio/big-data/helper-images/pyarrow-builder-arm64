ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ENV DEBIAN_FRONTEND "noninteractive"
ENV TERM "xterm"

RUN \
    set eux && \
    apt -qq update && \
    apt -qq -y install locales git wget python3 python3-setuptools python3-dev python3-pip wget cmake build-essential libjemalloc-dev libboost-dev libboost-filesystem-dev libboost-system-dev libboost-regex-dev autoconf flex bison flex pkg-config libtool libboost-test-dev libssl-dev libboost-program-options-dev libbz2-dev libreadline-dev libsqlite3-dev curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev && \
    dpkg-reconfigure locales && \
    apt -qq -y -o Dpkg::Options::="--force-confold" full-upgrade && \
    which python3 && \
    which pip3 && \
    update-alternatives --install /usr/bin/python python /usr/bin/python3 10 && \
    update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10 && \
    python --version && \
    pip --version && \
    gcc --version && \
    pip install scikit-build && \
    pip install --upgrade cython && \
    pip install --upgrade cmake six numpy pandas pytest wheel && \
    mkdir ./whls && \
    find /root/.cache/pip/wheels/ -name "*.whl" -exec cp {} whls \; && \
    ls -al ./whls && \
    pwd
